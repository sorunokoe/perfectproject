//
//  Pokemon.swift
//  PureProject
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//

import Foundation

struct Pokemon: Decodable{
    
    var name: String?
    var url: String?
    
    enum CodingKeys: CodingKey{
        case name
        case url
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try? container.decode(String.self, forKey: .name)
        self.url = try? container.decode(String.self, forKey: .url)
    }
    
}

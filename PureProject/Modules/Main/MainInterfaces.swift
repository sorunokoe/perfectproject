//
//  MainInterfaces.swift
//  PureProject
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//

enum MainCellReuseIdentifier: String{
    case pokemonTableView = "pokemonTableViewCellReuseIdentifier"
}
protocol MainViewControllerInterface{
    
}
protocol MainViewInterface{
    func setView()
    func addViews()
    func setConstrains()
}
protocol MainViewModelInterface{
    func getPokemonAbilities()
}


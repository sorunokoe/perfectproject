//
//  MainViewController.swift
//  PureProject
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class MainViewController: UIViewController{
    
    var viewModel: MainViewModelInterface?
    var customView: MainView!
    let disposeBag = DisposeBag()
    
    static func initilize(viewModel: MainViewModelInterface) -> MainViewController {
        let vc = MainViewController()
        vc.viewModel = viewModel
        return vc
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        customView = MainView(frame: self.view.frame)
        self.view = customView
        getPokemon()
        handleViewModel()
    }
   
    func getPokemon(){
        guard let viewModel = viewModel else { return }
        viewModel.getPokemonAbilities()
    }
    func handleViewModel(){
        guard let viewModel = viewModel as? MainViewModel else { return }
        viewModel.pokemons.asObservable()
            .bind(to: customView.pokemonTableView.rx.items(cellIdentifier: MainCellReuseIdentifier.pokemonTableView.rawValue, cellType: UITableViewCell.self)){ row, element, cell in
                    cell.textLabel!.text = element.name
                }
                .disposed(by: disposeBag)
    }
}
extension MainViewController: MainViewControllerInterface{
    
}

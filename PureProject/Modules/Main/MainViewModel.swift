//
//  MainViewModel.swift
//  PureProject
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//
import Moya
import RxSwift
import RxCocoa

class MainViewModel: MainViewModelInterface{
    
    let provider = MoyaProvider<PokemonAPI>()
    
    let disposeBag =  DisposeBag()
    
    private(set) var loading = BehaviorRelay<Bool>(value: false)
    let pokemons = BehaviorRelay<[Pokemon]>(value: [])
    
    func getPokemonAbilities(){
        provider.rx
            .request(.ability)
            .filterSuccessfulStatusCodes()
            .map(PokemonResponses<Pokemon>.self)
            .subscribe(onSuccess: {[weak self] (response) in
                guard let pokemons = response.results else { return }
                self?.pokemons.accept(pokemons)
            }) { (error) in
                print(error)
            }
            .disposed(by: disposeBag)
    }
    
}

//
//  MainView.swift
//  PureProject
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//

import UIKit
import PureLayout
import Hue

class MainView: UIView, MainViewInterface{
    
    var titleLabel: UILabel!
    var pokemonTableView: UITableView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setView()
        addViews()
        setConstrains()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setView() {
        self.backgroundColor = App.theme.colors.background
        titleLabel = {
            let label = UILabel()
            label.text = "Hello World"
            label.font = UIFont.systemFont(ofSize: 24, weight: .medium)
            label.textColor = App.theme.colors.text
            label.sizeToFit()
            return label
        }()
        pokemonTableView = {
            let tableView = UITableView()
            tableView.backgroundColor = App.theme.colors.background
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: MainCellReuseIdentifier.pokemonTableView.rawValue)
            return tableView
        }()
    }
    
    func addViews() {
        guard let titleLabel = titleLabel, let pokemonTableView = pokemonTableView else {
            return
        }
        [titleLabel, pokemonTableView].forEach{
            self.addSubview($0)
        }
    }
    
    func setConstrains() {
        guard let titleLabel = titleLabel, let pokemonTableView = pokemonTableView else {
            return
        }
        titleLabel.autoPinEdge(.top, to: .top, of: titleLabel.superview!, withOffset: 120)
        titleLabel.autoPinEdge(.left, to: .left, of: titleLabel.superview!, withOffset: 25)
        titleLabel.autoPinEdge(.right, to: .right, of: titleLabel.superview!, withOffset: -25)
        
        pokemonTableView.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: 25)
        pokemonTableView.autoPinEdge(.left, to: .left, of: pokemonTableView.superview!)
        pokemonTableView.autoPinEdge(.right, to: .right, of: pokemonTableView.superview!)
        pokemonTableView.autoPinEdge(.bottom, to: .bottom, of: pokemonTableView.superview!)
    }
}

//
//  PokemonResponses.swift
//  PureProject
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//

import Foundation

struct PokemonResponses<T: Decodable>: Decodable{
    var count: Int?
    var next: String?
    var previous: String?
    var results: [T]?
    
    enum CodingKeys: CodingKey{
        case count
        case next
        case previous
        case results
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.count = try? container.decode(Int.self, forKey: .count)
        self.next = try? container.decode(String.self, forKey: .next)
        self.previous = try? container.decode(String.self, forKey: .previous)
        self.results = try? container.decode([T].self, forKey: .results)
    }
    
    
}

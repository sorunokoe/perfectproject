//
//  PokemonAPI.swift
//  PureProject
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//

import Moya

public enum PokemonAPI{
    case ability
}
extension PokemonAPI: TargetType{
    
    public var baseURL: URL {
        return URL(string: "https://pokeapi.co/api/v2")!
    }
    
    public var path: String {
        switch self {
        case .ability: return "/ability"
        }
    }
    
    public var method: Method {
        switch self {
        case .ability: return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        return .requestPlain
//        let ts = "\(Date().timeIntervalSince1970)"
//        // 1
//        let hash = (ts + Marvel.privateKey + Marvel.publicKey).md5
//
//        // 2
//        let authParams = ["apikey": Marvel.publicKey, "ts": ts, "hash": hash]
//
//        switch self {
//        case .comics:
//            // 3
//            return .requestParameters(
//                parameters: [
//                    "format": "comic",
//                    "formatType": "comic",
//                    "orderBy": "-onsaleDate",
//                    "dateDescriptor": "lastWeek",
//                    "limit": 50] + authParams,
//                encoding: URLEncoding.default)
//        }
    }
    
    public var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    public var validationType: ValidationType{
        return .successCodes
    }
    
}

//
//  MainCoordinator.swift
//  PureProject
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//
import UIKit

class MainCoordinator: BaseCoordinatorInterface{
    
    static var shared: MainCoordinator? 
    
    var navigationController: UINavigationController!
    
    init(window: UIWindow){
        navigationController = UINavigationController()
        window.makeKeyAndVisible()
        window.rootViewController = navigationController
    }
    
    func navigate(to: CoordinatorTarget) {
        switch to {
        case .main:
            let vm = MainViewModel()
            let vc = MainViewController.initilize(viewModel: vm)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

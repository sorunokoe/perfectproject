//
//  BaseCoordinatorInterface.swift
//  PureProject
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//

enum CoordinatorTarget{
    case main
}
protocol BaseCoordinatorInterface {
    func navigate(to: CoordinatorTarget)
}

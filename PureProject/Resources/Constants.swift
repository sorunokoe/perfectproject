//
//  Constants.swift
//  PureProject
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//

import UIKit

struct Colors{
    var background: UIColor
    var text: UIColor
}

internal enum Theme{
    case dark, light
    
    var colors: Colors{
        switch self {
        case .dark:
            return Colors(background: .darkGray, text: .white)
        case .light:
            return Colors(background: .white, text: .darkGray)
        }
    }
}
internal struct App{

    static var theme: Theme = .dark
    
}

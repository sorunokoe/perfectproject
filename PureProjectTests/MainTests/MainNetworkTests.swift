//
//  MainNetworkTests.swift
//  PureProjectTests
//
//  Created by Salgara on 7/30/19.
//  Copyright © 2019 Salgara. All rights reserved.
//

import XCTest
@testable import PureProject

enum TestCases: String{
    case pokemonDataNotNil
}
class MainNetworkTests: XCTestCase {

    var sut: MainViewController!
    
    override func setUp() {
        super.setUp()
        let vm = MainViewModel()
        sut = MainViewController.initilize(viewModel: vm)
        _ = sut.view
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    func testFailed(with: TestCases){
        XCTFail(with.rawValue)
    }
    
    func test_getPokemonDataNotNil(){
        guard let viewModel = sut.viewModel as? MainViewModel else {
            testFailed(with: .pokemonDataNotNil)
            return
        }
        viewModel.getPokemonAbilities()
        let expect = expectation(description: "getPokemon request")
        viewModel.pokemons.subscribe { (event) in
            XCTAssertNotNil(event.element)
            expect.fulfill()
        }.dispose()
        waitForExpectations(timeout: 15) { (error) in
            if let error = error{
                XCTFail("waitForExpectations: \(error)")
            }
        }
        
    }

}
